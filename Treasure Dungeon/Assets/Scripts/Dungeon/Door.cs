﻿using UnityEngine;

public class Door : MonoBehaviour
{
	public KeyType requiredKeyType;

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Player")
		{
			IPlayer player = collision.gameObject.GetComponent<IPlayer>();
			if (player.HasKeyOf(requiredKeyType))
			{
				Destroy(gameObject);
			}
		}
	}
}
