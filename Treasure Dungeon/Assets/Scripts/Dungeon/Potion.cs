﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potion : MonoBehaviour
{
	[Range(1, 100)]
	public int healthPointsToBeRestored = 25;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "Player")
		{
			IPlayer player = collision.gameObject.GetComponent<IPlayer>();
			player.RestoreHealthPoints(healthPointsToBeRestored);
			Destroy(gameObject);
		}
	}
}
