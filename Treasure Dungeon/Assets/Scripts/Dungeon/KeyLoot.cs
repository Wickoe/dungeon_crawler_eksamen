﻿using UnityEngine;

public class KeyLoot : MonoBehaviour
{
	public KeyType keyType;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "Player")
		{
			IPlayer player = collision.gameObject.GetComponent<IPlayer>();
			player.LootKey(keyType);
			Destroy(gameObject);
		}
	}
}
