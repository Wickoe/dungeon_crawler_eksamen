﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaircaseTeleport : MonoBehaviour
{
	public Transform to;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "Player")
		{
			collision.gameObject.GetComponent<Transform>().position = to.position;
		}
	}
}
