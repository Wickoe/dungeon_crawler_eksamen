﻿using UnityEngine;

public class Life : MonoBehaviour
{
	[Range(1, 10)]
	public int livesToGain = 1;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "Player")
		{
			IPlayer player = collision.gameObject.GetComponent<IPlayer>();
			player.GainLives(livesToGain);
			Destroy(gameObject);
		}
	}
}
