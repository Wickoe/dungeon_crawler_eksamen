﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Minion : MonoBehaviour {
	public int bossHealthpointsModifier = 200;
	public int bossAttackPowerModifier = 15;
	public float bossAttackRangeModifier = 500f;

	void Start () {
		IEnemy enemyMinion = gameObject.GetComponent<IEnemy>();
		enemyMinion.AddBossModifiers(bossHealthpointsModifier, bossAttackPowerModifier, bossAttackRangeModifier);
	}
}
