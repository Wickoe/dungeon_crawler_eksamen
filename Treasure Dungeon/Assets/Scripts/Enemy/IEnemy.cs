﻿public interface IEnemy
{
	int MaximumHealthPoints();
	int ActualHealthpoints();
	void TakeDamage(int damageToTake);
	float GetTargetingRadius();
	int GetAttackPower();
	float GetAttackSpeed();

	void AddBossModifiers(int maxHealthModifier, int attackPowerModifier, float attackRadiusModifier);
}
