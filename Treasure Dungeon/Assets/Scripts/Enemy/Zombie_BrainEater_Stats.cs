﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie_BrainEater_Stats : MonoBehaviour, IEnemy
{
	private readonly int Death_Occures_At_Healthspoints = 0;

	public int maxHealthPoints = 100;
	public int actualHealthPoints;
	public int attackPower = 25;
	public float attackSpeed = 2f;
	public float attackRadius = 15f;

	private void Start()
	{
		actualHealthPoints = maxHealthPoints;
	}

	public int ActualHealthpoints()
	{
		return actualHealthPoints;
	}

	public void AddAttackPower(int toIncrease)
	{
		attackPower += toIncrease;
	}

	public void AddMaxHealthPoints(int toIncrease)
	{
		maxHealthPoints += toIncrease;
		actualHealthPoints = maxHealthPoints;
	}

	public int GetAttackPower()
	{
		return attackPower;
	}

	public float GetAttackSpeed()
	{
		return attackSpeed;
	}

	public float GetTargetingRadius()
	{
		return attackRadius;
	}

	public int MaximumHealthPoints()
	{
		return maxHealthPoints;
	}

	public void TakeDamage(int damageToTake)
	{
		actualHealthPoints -= damageToTake;
		if (actualHealthPoints <= Death_Occures_At_Healthspoints)
		{
			GameController.EnemyDied(gameObject);
		}
	}

	public void AddBossModifiers(int maxHealthModifier, int attackPowerModifier, float attackRadiusModifier)
	{
		maxHealthPoints += maxHealthModifier;
		actualHealthPoints = maxHealthPoints;
		attackPower += attackPowerModifier;
		attackRadius += attackRadiusModifier;
	}
}
