﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Actions : MonoBehaviour
{
	private IEnemy minionStats;
	private Transform minionPosition;

	private GameObject targetPlayer;
	private bool shouldAttackPlayer = false;

	public Transform projektileEntry;
	public GameObject projektile;
	public int projektileSpeed = 600;
	private readonly int Projektile_Time_To_Live = 10;

	private DateTime shootTakenAt = DateTime.Now;

	private readonly int Minimum_Distance_To_Walls_Check = 1;

	private void Start()
	{
		minionStats = gameObject.GetComponent<IEnemy>();
		minionPosition = gameObject.GetComponent<Transform>();
	}

	private void FixedUpdate()
	{
		CheckForTargetEnemy();
		AimForPlayer();
		Wander();
	}

	private void CheckForTargetEnemy()
	{
		List<RaycastHit2D> hits = new List<RaycastHit2D>();

		Vector2 originPosition;
		Vector2 direction;
		Vector2 tempMinionPossition = new Vector2(minionPosition.position.x, minionPosition.position.y);

		float x = 0f;
		float y = 0f;

		for (int degree = 0; degree < 360; degree += 45)
		{
			x = (float)(0.5 * Math.Cos(degree)) + minionPosition.position.x;
			y = (float)(0.5 * Math.Sin(degree)) + minionPosition.position.y;

			originPosition = new Vector2(x, y);
			direction = originPosition - tempMinionPossition;

			hits.Add(Physics2D.Raycast(originPosition, direction, minionStats.GetTargetingRadius()));
		}

		shouldAttackPlayer = WasPlayerFound(hits);
	}

	private bool WasPlayerFound(List<RaycastHit2D> hits)
	{
		var indexAt = 0;
		var playerHit = false;

		Collider2D collider;

		while (!playerHit && indexAt < hits.Count)
		{
			collider = hits[indexAt].collider;
			playerHit = collider != null && collider.tag == "Player";

			if (!playerHit)
			{
				indexAt++;
			}
		}

		if (playerHit)
		{
			targetPlayer = hits[indexAt].collider.gameObject;
		}

		return playerHit;
	}

	private void AimForPlayer()
	{
		if (shouldAttackPlayer)
		{
			var enemyMinionPosition = gameObject.GetComponent<Transform>().position;
			var playerPosition = targetPlayer.GetComponent<Transform>().position;
			var direction = (playerPosition - enemyMinionPosition).normalized;

			float degree = Vector2.Angle(new Vector2(1, 0), direction);

			if (direction.y < 0)
			{
				degree = 180 + (180 - degree);
			}

			degree = degree * Mathf.PI / 180;

			float x = (float)(Math.Cos(degree)) + minionPosition.position.x;
			float y = (float)(Math.Sin(degree)) + minionPosition.position.y;
			projektileEntry.position = new Vector3(x, y, 0);
			ShootPlayer();
		}
	}

	private void ShootPlayer()
	{
		if ((shootTakenAt.TimeOfDay - DateTime.Now.TimeOfDay).Seconds <= -minionStats.GetAttackSpeed())
		{
			shootTakenAt = DateTime.Now;
			GameObject projektile = Instantiate(this.projektile, projektileEntry.position, new Quaternion(), minionPosition);
			var direction = (targetPlayer.GetComponent<Transform>().position - minionPosition.position).normalized;
			projektile.GetComponent<Rigidbody2D>().AddForce(direction * projektileSpeed);
			projektile.GetComponent<BulletAction>().damage = minionStats.GetAttackPower();
			Destroy(projektile, Projektile_Time_To_Live);
		}
	}

	private void Wander()
	{
		List<RaycastHit2D> hits = new List<RaycastHit2D>();
		Vector2 originPosition;
		Vector2 direction;
		Vector2 tempMinionPossition = new Vector2(minionPosition.position.x, minionPosition.position.y);

		float x = 0f;
		float y = 0f;

		for (int degree = 0; degree < 360; degree += 90)
		{
			x = (float)(0.5 * Math.Cos(degree)) + minionPosition.position.x;
			y = (float)(0.5 * Math.Sin(degree)) + minionPosition.position.y;

			originPosition = new Vector2(x, y);
			direction = originPosition - tempMinionPossition;

			hits.Add(Physics2D.Raycast(originPosition, direction, Minimum_Distance_To_Walls_Check));
		}
		Walk(CheckForWalkableDirections(hits));
	}

	private bool[] CheckForWalkableDirections(List<RaycastHit2D> hits)
	{
		bool[] walkableDirections = new bool[hits.Count];
		int indexAt = 0;

		foreach (var rayHit in hits)
		{
			walkableDirections[indexAt] = hits[indexAt].collider == null;
			indexAt++;
		}

		return walkableDirections;
	}

	private void Walk(bool[] walkableDirections)
	{
		float rightWalkRange = 0;
		float upWalkRange = 0;
		float leftWalkRange = 0;
		float downWalkRange = 0;

		if (walkableDirections[0])
		{
			rightWalkRange = 1;
		}
		if (walkableDirections[1])
		{
			upWalkRange = 1;
		}
		if (walkableDirections[2])
		{
			leftWalkRange = -1;
		}
		if (walkableDirections[3])
		{
			downWalkRange = -1;
		}

		float x = UnityEngine.Random.Range(leftWalkRange, rightWalkRange) * 10;
		float y = UnityEngine.Random.Range(downWalkRange, upWalkRange) * 10;

		gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector3(x, y, 0));
	}
}
