﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy_UI : MonoBehaviour
{
	public GameObject enemy;
	public IEnemy enemyStats;
	public Text hpTextLabel;
	public Slider hpSlider;

	private void Start()
	{
		enemyStats = enemy.GetComponent<IEnemy>();
		hpTextLabel.text = "HP: " + enemyStats.ActualHealthpoints() + " / " + enemyStats.MaximumHealthPoints();
		hpSlider.maxValue = enemyStats.MaximumHealthPoints();
		hpSlider.value = enemyStats.ActualHealthpoints();
	}

	private void Update()
	{
		hpTextLabel.text = "HP: " + enemyStats.ActualHealthpoints() + " / " + enemyStats.MaximumHealthPoints();
		hpSlider.value = enemyStats.ActualHealthpoints();
	}
}
