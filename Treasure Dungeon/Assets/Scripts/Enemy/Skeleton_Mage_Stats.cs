﻿using UnityEngine;

public class Skeleton_Mage_Stats : MonoBehaviour, IEnemy
{
	public int maxHealthPoints = 50;
	private int actualHealthPoints;
	public float targetingRadius = 20;
	public int attackPower = 10;
	public float attackSpeed = 1f;
	private readonly int Death_Occures_At_Healthspoints = 0;
	public bool dividesOnDeath = true;
	public int on_Death_Division_Into = 3;

	private void Start()
	{
		actualHealthPoints = maxHealthPoints;
	}

	public int ActualHealthpoints()
	{
		return actualHealthPoints;
	}

	public int MaximumHealthPoints()
	{
		return maxHealthPoints;
	}

	public void TakeDamage(int damageToTake)
	{
		actualHealthPoints -= damageToTake;
		if (actualHealthPoints <= Death_Occures_At_Healthspoints)
		{
			if (dividesOnDeath)
			{
				for (int i = 0; i < on_Death_Division_Into; i++)
				{
					GameObject newEnemy = Instantiate(gameObject, gameObject.GetComponent<Transform>().position, new Quaternion());
					newEnemy.GetComponent<Skeleton_Mage_Stats>().dividesOnDeath = false;
				}
			}
			GameController.EnemyDied(gameObject);
		}
	}

	public float GetTargetingRadius()
	{
		return targetingRadius;
	}

	public int GetAttackPower()
	{
		return attackPower;
	}

	public float GetAttackSpeed()
	{
		return attackSpeed;
	}
	
	public void AddBossModifiers(int maxHealthModifier, int attackPowerModifier, float attackRadiusModifier)
	{
		maxHealthPoints += maxHealthModifier;
		actualHealthPoints = maxHealthPoints;
		attackPower += attackPowerModifier;
		targetingRadius += attackRadiusModifier;
	}
}
