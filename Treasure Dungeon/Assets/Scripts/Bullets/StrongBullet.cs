﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrongBullet : MonoBehaviour
{
	public GameObject projektile;
	public int ammonitionCount = 25;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "Player")
		{
			Player_Actions playerActions = collision.gameObject.GetComponent<Player_Actions>();
			playerActions.activeBullet = projektile;
			playerActions.ammoLeft = ammonitionCount;
			Destroy(gameObject);
		}
	}
}
