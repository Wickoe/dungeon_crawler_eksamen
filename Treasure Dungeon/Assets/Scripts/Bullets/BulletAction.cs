﻿using UnityEngine;

public class BulletAction : MonoBehaviour
{
	public int damage = 10;

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Enemy")
		{
			var enemy = collision.gameObject.GetComponent<IEnemy>();
			enemy.TakeDamage(damage);
		} else if(collision.gameObject.tag == "Player")
		{
			var player = collision.gameObject.GetComponent<IPlayer>();
			player.TakeDamage(damage);
		}

		Destroy(gameObject);
	}
}
