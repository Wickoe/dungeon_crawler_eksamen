﻿using System.Collections;
using System.Collections.Generic;

public interface IPlayer
{
	// Returns the maximum healthpoints the player can have at any moment in time.
	int GetMaxHealthPoints();
	/* Returns the actual healthpoints the player has at any moment in time.
	 * At any moment the player's healthpoints cannot be any greater than the player's max healthpoints.
	 * At zero healthpoints the player should loos a life.
	 */
	int GetActualHealthPoints();
	// Returns the number of lives the player has left.
	int GetNumberOfLives();
	// The player takes damage equal to the damage the hitting projectile does.
	void TakeDamage(int damageToTake);
	// Restore a number of healthpoints for the player.
	void RestoreHealthPoints(int healthPointsToRestore);
	// Restore a specific number of lives (usually 1).
	void GainLife();
	// Restore a random number of lives to the player. 
	void GainLives(int livesToGain);
	// The player died and lost a life. 
	void LooseLife();
	void LootKey(KeyType keyType);
	bool HasKeyOf(KeyType aKeyType);
	List<KeyType> GetLootedKeys();
}
