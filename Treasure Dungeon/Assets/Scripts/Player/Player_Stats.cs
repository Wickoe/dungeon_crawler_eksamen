﻿using System.Collections.Generic;
using UnityEngine;

public class Player_Stats : MonoBehaviour, IPlayer
{
	[Range(1, 10)]
	public int lives = 3;
	private int maxHealthPoints = 100;
	private int actualHealthPoints;
	private readonly int Death_Occures_At_Healthpoints = 0;

	public GameObject gameControllerObj;
	private GameController gameController;
	public List<KeyType> keys = new List<KeyType>();

	// Use this for initialization
	void Start()
	{
		actualHealthPoints = maxHealthPoints;
		gameController = gameControllerObj.GetComponent<GameController>();
	}

	public int GetActualHealthPoints()
	{
		return actualHealthPoints;
	}

	public int GetMaxHealthPoints()
	{
		return maxHealthPoints;
	}

	public void TakeDamage(int damageToTake)
	{
		actualHealthPoints -= damageToTake;
		if (actualHealthPoints <= Death_Occures_At_Healthpoints)
		{
			LooseLife();
		}
	}

	public void RestoreHealthPoints(int healthPointsToRestore)
	{
		actualHealthPoints += healthPointsToRestore;
		if (actualHealthPoints > maxHealthPoints)
		{
			actualHealthPoints = maxHealthPoints;
		}
	}

	public void GainLife()
	{
		GainLives(1);
	}

	public void GainLives(int livesToGain)
	{
		lives += livesToGain;
	}

	public void LooseLife()
	{
		lives -= 1;
		actualHealthPoints = maxHealthPoints;
		gameController.LifeLost(lives);
	}

	public int GetNumberOfLives()
	{
		return lives;
	}

	public void LootKey(KeyType keyType)
	{
		keys.Add(keyType);
	}

	public bool HasKeyOf(KeyType aKeyType)
	{
		bool containsKey = keys.Contains(aKeyType);
		if (containsKey)
		{
			keys.Remove(aKeyType);
		}

		return containsKey;
	}

	public List<KeyType> GetLootedKeys()
	{
		return keys;
	}
}
