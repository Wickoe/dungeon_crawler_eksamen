﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Player_UI : MonoBehaviour
{
	public GameObject player;
	private IPlayer playerStats;
	private Player_Actions playerActions;
	public Text hpBarTextLabel;
	public Slider hpBar;
	public Text livesTextLabel;
	public Text killCounterTextLabel;
	public Image bulletDisplay;
	public Text bulletAmmunition;

	public Text yellowKeysColletedLabel;
	public Text redKeysColletedLabel;
	public Text blueKeysColletedLabel;


	// Use this for initialization
	void Start()
	{
		playerStats = player.GetComponent<IPlayer>();
		playerActions = player.GetComponent<Player_Actions>();
		hpBarTextLabel.text = "HP: " + playerStats.GetActualHealthPoints() + " / " + playerStats.GetMaxHealthPoints();
		hpBar.maxValue = playerStats.GetMaxHealthPoints();
		hpBar.value = playerStats.GetActualHealthPoints();
		livesTextLabel.text = "" + playerStats.GetNumberOfLives();
		killCounterTextLabel.text = "Kill count: " + GameController.playerKillCount;
	}

	private void FixedUpdate()
	{
		hpBar.value = playerStats.GetActualHealthPoints();
		hpBarTextLabel.text = "HP: " + playerStats.GetActualHealthPoints() + " / " + playerStats.GetMaxHealthPoints();
		livesTextLabel.text = "" + playerStats.GetNumberOfLives();
		killCounterTextLabel.text = "Kill count: " + GameController.playerKillCount;
		bulletAmmunition.text = "" + playerActions.ammoLeft;
		bulletDisplay.sprite = playerActions.activeBullet.GetComponent<SpriteRenderer>().sprite;

		if (playerActions.ammoLeft < 0)
		{
			bulletAmmunition.text = "Infinite";
		}
		setCollectedKeys();
	}

	private void setCollectedKeys()
	{
		int blueKeys = 0;
		int redKeys = 0;
		int yellowKeys = 0;

		foreach (var key in playerStats.GetLootedKeys())
		{
			if (key == KeyType.RedKey)
			{
				redKeys++;
			}
			else if (key == KeyType.BlueKey)
			{
				blueKeys++;
			}
			else
			{
				yellowKeys++;
			}

			yellowKeysColletedLabel.text = "" + yellowKeys;
			redKeysColletedLabel.text = "" + redKeys;
			blueKeysColletedLabel.text = "" + blueKeys;
		}
	}
}
