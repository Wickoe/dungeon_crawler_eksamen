﻿using System;
using UnityEngine;

public class Player_Actions : MonoBehaviour
{
	public GameObject default_Projektile;

	private Rigidbody2D rBody;
	private float horizontalWalk = 0f;
	private float verticalWalk = 0f;

	public GameObject activeBullet;
	public Transform bulletEntry;
	public int bulletSpeed = 150;
	public int ammoLeft = -1;

	private void Start()
	{
		rBody = gameObject.GetComponent<Rigidbody2D>();
	}

	// Update is called once per frame
	void Update()
	{
		MoveActions();
		BulletEntryPosition();
		FireAction();

		if (ammoLeft == 0)
		{
			activeBullet = default_Projektile;
			ammoLeft--;
		}
	}

	private void FixedUpdate()
	{
		rBody.AddForce(transform.up * verticalWalk, ForceMode2D.Impulse);
		rBody.AddForce(transform.right * horizontalWalk, ForceMode2D.Impulse);
		verticalWalk = 0;
		horizontalWalk = 0;
	}

	private void MoveActions()
	{
		horizontalWalk = Input.GetAxis("Horizontal");
		verticalWalk = Input.GetAxis("Vertical");
	}

	private void BulletEntryPosition()
	{
		var cameraPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		var playerPosition = gameObject.GetComponent<Transform>().position;
		var direction = (cameraPosition - playerPosition).normalized;

		float degree = Vector2.Angle(new Vector2(1, 0), direction);

		if (direction.y < 0)
		{
			degree = 180 + (180 - degree);
		}

		degree = degree * Mathf.PI / 180;

		float x = (float)(Math.Cos(degree)) + playerPosition.x;
		float y = (float)(Math.Sin(degree)) + playerPosition.y;
		bulletEntry.position = new Vector3(x, y, 0);
	}

	private void FireAction()
	{
		if (Input.GetButtonDown("Fire1"))
		{
			Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector3 playerPosition = gameObject.transform.position;

			GameObject bullet = Instantiate(activeBullet, bulletEntry.position, new Quaternion(), gameObject.transform);
			Destroy(bullet, 10);

			var bulletDirection = new Vector2(mousePosition.x - playerPosition.x, mousePosition.y - playerPosition.y).normalized;
			Rigidbody2D bulletRigid = bullet.GetComponent<Rigidbody2D>();
			bulletRigid.AddForce(bulletDirection * bulletSpeed + rBody.velocity);

			ShootTaken();
		}
	}

	private void ShootTaken()
	{
		if (ammoLeft > 0)
		{
			ammoLeft--;
		}
	}
}
