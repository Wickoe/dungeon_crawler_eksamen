﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
	public static int playerKillCount = 0;
	public static bool gameWon = false;

	public GameObject panel;
	public Text deathMessage;
	public Button retryBtn;
	public Text retryBtnText;
	public readonly int Game_Over_At_Lives_Left = 0;

	private bool restartGame = false;

	public Transform checkpoint;
	public GameObject player;

	private DateTime gameStarted = DateTime.Now;

	private void Start()
	{
		panel.SetActive(false);
	}

	private void Update()
	{
		if (gameWon)
		{
			gameWon = false;
			GameWon();
		}
	}

	public void LifeLost(int livesLeft)
	{
		// Set timescale to 'pause' game
		Time.timeScale = 0;
		if (livesLeft > Game_Over_At_Lives_Left)
		{
			deathMessage.text = "Du døde til et monster";
			retryBtnText.text = "Prøv igen";
		}
		else
		{
			deathMessage.text = "Godt forsøg men du har tabt spillet";
			retryBtnText.text = "Start spillet igen";
		}

		restartGame = livesLeft <= Game_Over_At_Lives_Left;
		panel.SetActive(true);
	}

	public void btnRetryClicked()
	{
		Time.timeScale = 1;
		panel.SetActive(false);
		if (restartGame)
		{
			restartGame = false;
			SceneManager.LoadScene(0);
		}
		else
		{
			player.GetComponent<Transform>().position = checkpoint.position;
		}
	}

	public void SetCheckpoint(Transform position)
	{
		checkpoint = position;
	}

	public static void EnemyDied(GameObject aGameObject)
	{
		playerKillCount++;
		gameWon = aGameObject.GetComponent<Boss_Minion>() != null;
		Destroy(aGameObject);
	}

	private void GameWon()
	{
		Time.timeScale = 0;
		TimeSpan timeTaken = DateTime.Now - gameStarted;
		deathMessage.text = "Du har vundet spillet med " + playerKillCount + " kills på " + timeTaken.Hours + " timer, " + timeTaken.Minutes + " min. og " + timeTaken.Seconds + " sekunder";
		retryBtnText.text = "Start spillet igen";
		restartGame = true;
		panel.SetActive(true);
		playerKillCount = 0;
	}
}
